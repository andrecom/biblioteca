package biblioteca;

public abstract class Empleado {
	
	private String identificacion;
	private String nombre;
	private String horasTrabajadas;
	private String salario;
	
	// encapsulamiento nombre
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	// encapsulamiento identificacion 
	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	
	// encapsulamiento horasTrabajadas
	public String getHorasTrabajadas() {
		return horasTrabajadas;
	}
	
	public void setHorasTrabajadas(String horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}
	
	//	encapsulamiento salario
	public String getSalario() {
		return salario;
	}

	public void setSalario(String salario) {
		this.salario = salario;
	}
 	
	
	public abstract String trabajar(String trabajo);
}
