package biblioteca;

import java.io.IOException;

public class PersonalAseo extends Empleado implements LegislacionLaboralChile {
	
	private String rondaLimpieza;
	
	PersonalAseo(){
		rondaLimpieza = "No se ha limpiado nada";
	}
	
	public String getRondaLimpieza() {
		return rondaLimpieza;
	}
	
	@Override
	public String trabajar(String lugar) {
		rondaLimpieza += " \n"+  lugar;
		return rondaLimpieza;
	}
	
	@Override
	public int calcularSalario(int horas) {
		return horas*1600;
	}
	
	public int calcularSalario(int horas, int precioXHora) {
		return horas*precioXHora;
	}
}
