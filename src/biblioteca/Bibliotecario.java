package biblioteca;

public class Bibliotecario extends Empleado implements LegislacionLaboralChile {
	
	private String librosPrestados;
	
	Bibliotecario() {
		librosPrestados = "No se ha prestado ning�n libro";
	}
	
	@Override
	public String trabajar(String libro) {
		librosPrestados +=  "\n"+libro;
			return librosPrestados;
	}
	
	@Override
	public int calcularSalario(int horas) {
		return horas*2500;
	}
	
	public int calcularSalario(int horas, int precioXHora) {
		return horas*precioXHora;
	}
	
	

}
