package biblioteca;

import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Principal {

	public static void main(String[] args) {

		/*
		 * PersonalAseo persona01 = new PersonalAseo();
		 * persona01.setIdentificacion("Juan Perez");
		 */
		// return identificacion;

		// Ejemplo 01 de uso de ArrayList
		/*
		 * ArrayList<String> datos = new ArrayList<>();
		 * 
		 * datos.add("primer dato"); datos.add("segundo dato");
		 * datos.add("tercer dato"); datos.add("cuarto dato");
		 * 
		 * for(String dato: datos) { System.out.println(dato); }
		 */
		// Muestra por consola los 4 datos

		// Ejemplo 02
		/*
		 * ArrayList<Bibliotecario> datos = new ArrayList<>();
		 * 
		 * Bibliotecario bibli = new Bibliotecario();
		 * 
		 * for (int i=0; i<2; i++) {
		 * bibli.setIdentificacion(JOptionPane.showInputDialog("Ingrese identificacion")
		 * ); bibli.setNombre(JOptionPane.showInputDialog("Ingrese nombre"));
		 * datos.add(bibli); }
		 */
		// Te pide por cajo de texto los datos.

		// Ejercicio 27-11-2018 ==============
		/*
		 * ArrayList<PersonalAseo> datos = new ArrayList<PersonalAseo>();
		 * 
		 * for (int i=0; i< 3; i++) { PersonalAseo nombres = new PersonalAseo();
		 * nombres.setIdentificacion("123"); nombres.setNombre("Pedrito");
		 * datos.add(nombres); }
		 * 
		 * for(PersonalAseo pepe: datos) {
		 * System.out.println("--->"+(pepe.getNombre()));
		 * System.out.println("--->"+(pepe.getIdentificacion())); }
		 */

		// ==============================

		// un objeto
		PersonalAseo unPA = new PersonalAseo();
		// otro objeto
		PersonalAseo dosPA = new PersonalAseo();

		Bibliotecario unB = new Bibliotecario();

		/*
		 * M�todos para imprimir los valores(atributos) de los objetos
		 * System.out.println(unPA.trabajar("Sala")); unPA.trabajar("Sala");
		 * unPA.trabajar("Cocina"); System.out.println(unPA.trabajar("Ba�o"));
		 * System.out.println(unB.trabajar("Harry Potter"));
		 */

//			unB.calcularSalario(horas);
//			JOptionPane.showMessageDialog(null,unB.getSalario());
		
//		System.out.println(unB.calcularSalario(120));
//		System.out.println(unPA.calcularSalario(166));

		Object[] options1 = { "bibliotecario", "CANCEL" };
		 int opcion = JOptionPane.showOptionDialog(null, "Click OK to continue", "Warning",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options1, options1[0]);
		System.out.println(opcion);

		
		Object options[] = { "Bibliotecario", "Personal de Aseo" };
		int horas = Integer.parseInt(JOptionPane.showInputDialog(""
		+ "Introduzca las"
		+ "Horas que trabajo"));
		int precioXHora = Integer.parseInt(JOptionPane.showInputDialog(""
		+ "Introduzca el precio por hora"));
		
	}

}
